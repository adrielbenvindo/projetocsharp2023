﻿using AutoMapper;
﻿using ProjectMVC2023.Models;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023
{
    public class AutoMapperConfig : Profile
    {
        public static MapperConfiguration RegisterMappings()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tamanho, TamanhoModel>();
                cfg.CreateMap<TamanhoModel, Tamanho>();
                cfg.CreateMap<Maybe<Tamanho>, Maybe<TamanhoModel>>();
                cfg.CreateMap<Maybe<TamanhoModel>, Maybe<Tamanho>>();
                cfg.CreateMap<Sala, SalaModel>();
                cfg.CreateMap<SalaModel, Sala>();
                cfg.CreateMap<Maybe<Sala>, Maybe<SalaModel>>();
                cfg.CreateMap<Maybe<SalaModel>, Maybe<Sala>>();
                cfg.CreateMap<Ator, AtorModel>();
                cfg.CreateMap<AtorModel, Ator>();
                cfg.CreateMap<Maybe<Ator>, Maybe<AtorModel>>();
                cfg.CreateMap<Maybe<AtorModel>, Maybe<Ator>>();
                cfg.CreateMap<Cadeira, CadeiraModel>();
                cfg.CreateMap<CadeiraModel, Cadeira>();
                cfg.CreateMap<Maybe<Cadeira>, Maybe<CadeiraModel>>();
                cfg.CreateMap<Maybe<CadeiraModel>, Maybe<Cadeira>>();
                cfg.CreateMap<Categoria, CategoriaModel>();
                cfg.CreateMap<CategoriaModel, Categoria>();
                cfg.CreateMap<Maybe<Categoria>, Maybe<CategoriaModel>>();
                cfg.CreateMap<Maybe<CategoriaModel>, Maybe<Categoria>>();
                cfg.CreateMap<Divisao, DivisaoModel>();
                cfg.CreateMap<DivisaoModel, Divisao>();
                cfg.CreateMap<Maybe<Divisao>, Maybe<DivisaoModel>>();
                cfg.CreateMap<Maybe<DivisaoModel>, Maybe<Divisao>>();
                cfg.CreateMap<Evento, EventoModel>();
                cfg.CreateMap<EventoModel, Evento>();
                cfg.CreateMap<Maybe<Evento>, Maybe<EventoModel>>();
                cfg.CreateMap<Maybe<EventoModel>, Maybe<Evento>>();
                cfg.CreateMap<Sessao, SessaoModel>();
                cfg.CreateMap<SessaoModel, Sessao>();
                cfg.CreateMap<Maybe<Sessao>, Maybe<SessaoModel>>();
                cfg.CreateMap<Maybe<SessaoModel>, Maybe<Sessao>>();
                cfg.CreateMap<Tipo, TipoModel>();
                cfg.CreateMap<TipoModel, Tipo>();
                cfg.CreateMap<Maybe<Tipo>, Maybe<TipoModel>>();
                cfg.CreateMap<Maybe<TipoModel>, Maybe<Tipo>>();
                cfg.CreateMap<Usuario, UsuarioModel>();
                cfg.CreateMap<UsuarioModel, Usuario>();
                cfg.CreateMap<Maybe<Usuario>, Maybe<UsuarioModel>>();
                cfg.CreateMap<Maybe<UsuarioModel>, Maybe<Usuario>>();

            });

            return config;
        }
    }

}
