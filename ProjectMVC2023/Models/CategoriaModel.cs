﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class CategoriaModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }




        public CategoriaModel save(CategoriaModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Categoria categoria = mapper.Map<Categoria>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                CategoriaRepository repository = new CategoriaRepository(context);

                if (model.id == 0)
                    repository.Insert(categoria);
                else
                    repository.Update(categoria);

                context.SaveChanges();
            }
            model.id = categoria.id;
            return model;
            
        
        }


        public List<CategoriaModel> list() {
            List<CategoriaModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                CategoriaRepository repository = new CategoriaRepository(context);
                List<Categoria> list= repository.ListAll();
                listmodel = mapper.Map<List<CategoriaModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<CategoriaModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                CategoriaRepository repository = new CategoriaRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Categoria> categoria= repository.Recover(c=>c.id==id);
                return mapper.Map<Maybe<CategoriaModel>>(categoria);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                CategoriaRepository repository = new CategoriaRepository(context);
                Maybe<Categoria> categoria = repository.Recover(c => c.id == id);
                if (categoria.TryGetValue(out Categoria data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
