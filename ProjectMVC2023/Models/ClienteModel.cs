﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class ClienteModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Descrição deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Descrição deve ter no mínimo 3 caracteres")]
        [Display(Name ="Descrição")]
        public String nome { get; set; }
        
        [Display(Name ="Email")]
        public String email { get; set; }
        
        public int LoginId { get; set; }

        [Display(Name ="Nome de Usuário")]
        public String username { get; set; }

        [Display(Name ="Senha")]
        public String password { get; set; }




        public ClienteModel save(ClienteModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Cliente cliente = mapper.Map<Cliente>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                ClienteRepository repository = new ClienteRepository(context);

                if (model.id == 0)
                    repository.Insert(cliente);
                else
                    repository.Update(cliente);

                context.SaveChanges();
            }
            model.id = cliente.id;
            return model;
            
        
        }


        public List<ClienteModel> list() {
            List<ClienteModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                ClienteRepository repository = new ClienteRepository(context);
                List<Cliente> list= repository.ListAll();
                listmodel = mapper.Map<List<ClienteModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<ClienteModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                ClienteRepository repository = new ClienteRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Cliente> cliente= repository.Recover(c=>c.id==id);
                return mapper.Map<Maybe<ClienteModel>>(cliente);
            }
        }

        public Maybe<ClienteModel> selectByLogin(int loginId) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                ClienteRepository repository = new ClienteRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Cliente> cliente= repository.Recover(c=>c.LoginId==loginId);
                return mapper.Map<Maybe<ClienteModel>>(cliente);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                ClienteRepository repository = new ClienteRepository(context);
                Maybe<Cliente> cliente = repository.Recover(c => c.id == id);
                if (cliente.TryGetValue(out Cliente data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
