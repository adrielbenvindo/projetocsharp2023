﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;
using ProjectMVC2023;

namespace ProjectMVC2023.Models
{
    public class AtorModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }
        
        [Display(Name ="Idade")]
        public int idade { get; set; }

        [Display(Name ="Papel")]
        public String papel { get; set; }

        public int EventoId { get; set; }
        
        public String foto { get; set; }
        [Required(ErrorMessage = "Selecione uma imagem para cartaz")]
        public IFormFile fileImage { get; set; }


        public AtorModel save(AtorModel model, IWebHostEnvironment webHostEnvironment)
        {
            //Categoria sala = new Categoria();
            //sala.id = model.id;
            //sala.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Ator ator = mapper.Map<Ator>(model);
if((FileHandle.Upload(Maybe.Some<IFormFile>(model.fileImage),Maybe.Some<String>("Atores"),Maybe.Some<String>("ator"),Maybe.Some<String>("foto"),Maybe.
Some < IWebHostEnvironment>(webHostEnvironment))).TryGetValue(out string data))
                ator.foto = data;
            else
                ator.foto = "";

            using (OrganizationContext context = new OrganizationContext())
            {
                AtorRepository repository = new AtorRepository(context);

                if (model.id == 0)
                    repository.Insert(ator);
                else
                    repository.Update(ator);

                context.SaveChanges();
            }
            model.id = ator.id;
            return model;

        }


        public List<AtorModel> list()
        {
            List<AtorModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                AtorRepository repository = new AtorRepository(context);
                List<Ator> list = repository.ListAll();
                listmodel = mapper.Map<List<AtorModel>>(list);
            }

            return listmodel;
        }

        public List<AtorModel> listRelated(int eventoId)
        {
            List<AtorModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                AtorRepository repository = new AtorRepository(context);
                List<Ator> list = repository.List(a => a.EventoId == eventoId);
                listmodel = mapper.Map<List<AtorModel>>(list);
            }

            return listmodel;
        }

        public Maybe<AtorModel> select(int id)
        {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                AtorRepository repository = new AtorRepository(context);
                //select * from salaegoria c where c.id = id
                Maybe<Ator> ator = repository.Recover(a => a.id == id);
                return mapper.Map<Maybe<AtorModel>>(ator);
                
            }
        }

        public void delete(int id)
        {

            using (OrganizationContext context = new OrganizationContext())
            {
                AtorRepository repository = new AtorRepository(context);
                Maybe<Ator> ator = repository.Recover(a => a.id == id);
                if (ator.TryGetValue(out Ator data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
