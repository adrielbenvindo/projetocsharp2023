﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class CadeiraModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }

        public int SalaId { get; set; }

        public String nomeSala { get; set; }


        public CadeiraModel save(CadeiraModel model)
        {

            //Categoria sala = new Categoria();
            //sala.id = model.id;
            //sala.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Cadeira cadeira = mapper.Map<Cadeira>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                CadeiraRepository repository = new CadeiraRepository(context);

                if (model.id == 0)
                    repository.Insert(cadeira);
                else
                    repository.Update(cadeira);

                context.SaveChanges();
            }
            model.id = cadeira.id;
            return model;


        }


        public List<CadeiraModel> list()
        {
            List<CadeiraModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                CadeiraRepository repository = new CadeiraRepository(context);
                List<Cadeira> list = repository.ListAll();
                listmodel = mapper.Map<List<CadeiraModel>>(list);
            }

            return listmodel;
        }

        public List<CadeiraModel> listRelated(int salaId)
        {
            List<CadeiraModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                CadeiraRepository repository = new CadeiraRepository(context);
                List<Cadeira> list = repository.List(c => c.SalaId == salaId);
                listmodel = mapper.Map<List<CadeiraModel>>(list);
            }

            return listmodel;
        }

        public Maybe<CadeiraModel> select(int id)
        {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                CadeiraRepository repository = new CadeiraRepository(context);
                //select * from salaegoria c where c.id = id
                Maybe<Cadeira> cadeira = repository.Recover(c => c.id == id);
                return mapper.Map<Maybe<CadeiraModel>>(cadeira);
            }
        }

        public void delete(int id)
        {

            using (OrganizationContext context = new OrganizationContext())
            {
                CadeiraRepository repository = new CadeiraRepository(context);
                Maybe<Cadeira> cadeira = repository.Recover(c => c.id == id);
                if (cadeira.TryGetValue(out Cadeira data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
