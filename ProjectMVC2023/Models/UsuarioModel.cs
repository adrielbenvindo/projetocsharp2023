﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class UsuarioModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Descrição deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Descrição deve ter no mínimo 3 caracteres")]
        [Display(Name ="Descrição")]
        public String nome { get; set; }
        
        [Display(Name ="É administrador?")]
        public bool is_admin { get; set; }

        [Display(Name ="É gerenciador de eventos?")]
        public bool is_event_manager { get; set; }
        
        public int LoginId { get; set; }

        [Display(Name ="Nome de Usuário")]
        public String username { get; set; }

        [Display(Name ="Senha")]
        public String password { get; set; }



        public UsuarioModel save(UsuarioModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Usuario usuario = mapper.Map<Usuario>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                UsuarioRepository repository = new UsuarioRepository(context);

                if (model.id == 0)
                    repository.Insert(usuario);
                else
                    repository.Update(usuario);

                context.SaveChanges();
            }
            model.id = usuario.id;
            return model;
            
        
        }


        public List<UsuarioModel> list() {
            List<UsuarioModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                UsuarioRepository repository = new UsuarioRepository(context);
                List<Usuario> list= repository.ListAll();
                listmodel = mapper.Map<List<UsuarioModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<UsuarioModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                UsuarioRepository repository = new UsuarioRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Usuario> usuario= repository.Recover(u=>u.id==id);
                return mapper.Map<Maybe<UsuarioModel>>(usuario);
            }
        }

        public Maybe<UsuarioModel> selectByLogin(int loginId) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                UsuarioRepository repository = new UsuarioRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Usuario> usuario= repository.Recover(u=>u.LoginId==loginId);
                return mapper.Map<Maybe<UsuarioModel>>(usuario);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                UsuarioRepository repository = new UsuarioRepository(context);
                Maybe<Usuario> usuario = repository.Recover(u => u.id == id);
                if (usuario.TryGetValue(out Usuario data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
