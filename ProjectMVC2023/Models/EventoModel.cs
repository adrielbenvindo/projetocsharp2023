﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;
using ProjectMVC2023;

namespace ProjectMVC2023.Models
{
    public class EventoModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }
        
        [Display(Name ="Duração")]
        public int duracao { get; set; }

        [Display(Name ="Restrição de idade")]
        public int restricao_idade { get; set; }

        public int TipoId { get; set; }
        
        public String cartaz { get; set; }
        [Required(ErrorMessage = "Selecione uma imagem para cartaz")]
        public IFormFile fileImage { get; set; }


        public EventoModel save(EventoModel model, IWebHostEnvironment webHostEnvironment)
        {

            //Categoria sala = new Categoria();
            //sala.id = model.id;
            //sala.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Evento evento = mapper.Map<Evento>(model);
if((FileHandle.Upload(Maybe.Some<IFormFile>(model.fileImage),Maybe.Some<String>("Cartazes"),Maybe.Some<String>("evento"),Maybe.Some<String>("cartaz"),
Maybe . Some<IWebHostEnvironment>(webHostEnvironment))).TryGetValue(out string data))
                evento.cartaz = data;
            else
                evento.cartaz = "";

            using (OrganizationContext context = new OrganizationContext())
            {
                EventoRepository repository = new EventoRepository(context);

                if (model.id == 0)
                    repository.Insert(evento);
                else
                    repository.Update(evento);

                context.SaveChanges();
            }
            model.id = evento.id;
            return model;


        }


        public List<EventoModel> list()
        {
            List<EventoModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                EventoRepository repository = new EventoRepository(context);
                List<Evento> list = repository.ListAll();
                listmodel = mapper.Map<List<EventoModel>>(list);
            }

            return listmodel;
        }

        public Maybe<EventoModel> select(int id)
        {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                EventoRepository repository = new EventoRepository(context);
                //select * from salaegoria c where c.id = id
                Maybe<Evento> evento = repository.Recover(e => e.id == id);
                return mapper.Map<Maybe<EventoModel>>(evento);
            }
        }

        public void delete(int id)
        {

            using (OrganizationContext context = new OrganizationContext())
            {
                EventoRepository repository = new EventoRepository(context);
                Maybe<Evento> evento = repository.Recover(e => e.id == id);
                if (evento.TryGetValue(out Evento data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
