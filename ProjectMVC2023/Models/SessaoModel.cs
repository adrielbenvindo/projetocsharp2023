﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class SessaoModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }
        
        [Display(Name ="Valor")]
        public Decimal valor { get; set; }

        [Display(Name ="Data e Hora de inicio")]
        public DateTime data_hora_inicio { get; set; }

        [Display(Name ="Data e Hora de fim")]
        public DateTime data_hora_fim { get; set; }

        public int EventoId { get; set; }
        public int SalaId { get; set; }

        public String nomeEvento { get; set; }
        public String nomeSala { get; set; }


        public SessaoModel save(SessaoModel model)
        {

            //Categoria sala = new Categoria();
            //sala.id = model.id;
            //sala.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Sessao sessao = mapper.Map<Sessao>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                SessaoRepository repository = new SessaoRepository(context);

                if (model.id == 0)
                    repository.Insert(sessao);
                else
                    repository.Update(sessao);

                context.SaveChanges();
            }
            model.id = sessao.id;
            return model;


        }


        public List<SessaoModel> list()
        {
            List<SessaoModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                SessaoRepository repository = new SessaoRepository(context);
                List<Sessao> list = repository.ListAll();
                listmodel = mapper.Map<List<SessaoModel>>(list);
            }

            return listmodel;
        }

        public List<SessaoModel> listRelated(int eventoId)
        {
            List<SessaoModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                SessaoRepository repository = new SessaoRepository(context);
                List<Sessao> list = repository.List(s => s.EventoId == eventoId);
                listmodel = mapper.Map<List<SessaoModel>>(list);
            }

            return listmodel;
        }

        public Maybe<SessaoModel> select(int id)
        {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                SessaoRepository repository = new SessaoRepository(context);
                //select * from salaegoria c where c.id = id
                Maybe<Sessao> sessao = repository.Recover(s => s.id == id);
                return mapper.Map<Maybe<SessaoModel>>(sessao);
            }
        }

        public void delete(int id)
        {

            using (OrganizationContext context = new OrganizationContext())
            {
                SessaoRepository repository = new SessaoRepository(context);
                Maybe<Sessao> sessao = repository.Recover(s => s.id == id);
                if (sessao.TryGetValue(out Sessao data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
