﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class SalaModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }

        public int TamanhoId { get; set; }


        public SalaModel save(SalaModel model)
        {

            //Categoria sala = new Categoria();
            //sala.id = model.id;
            //sala.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Sala sala = mapper.Map<Sala>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                SalaRepository repository = new SalaRepository(context);

                if (model.id == 0)
                    repository.Insert(sala);
                else
                    repository.Update(sala);

                context.SaveChanges();
            }
            model.id = sala.id;
            return model;


        }


        public List<SalaModel> list()
        {
            List<SalaModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                SalaRepository repository = new SalaRepository(context);
                List<Sala> list = repository.ListAll();
                listmodel = mapper.Map<List<SalaModel>>(list);
            }

            return listmodel;
        }

        public Maybe<SalaModel> select(int id)
        {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                SalaRepository repository = new SalaRepository(context);
                //select * from salaegoria c where c.id = id
                Maybe<Sala> sala = repository.Recover(s => s.id == id);
                return mapper.Map<Maybe<SalaModel>>(sala);
            }
        }

        public void delete(int id)
        {

            using (OrganizationContext context = new OrganizationContext())
            {
                SalaRepository repository = new SalaRepository(context);
                Maybe<Sala> sala = repository.Recover(s => s.id == id);
                if (sala.TryGetValue(out Sala data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
