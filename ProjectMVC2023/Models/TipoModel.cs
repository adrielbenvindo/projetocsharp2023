using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class TipoModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }




        public TipoModel save(TipoModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Tipo tipo = mapper.Map<Tipo>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                TipoRepository repository = new TipoRepository(context);

                if (model.id == 0)
                    repository.Insert(tipo);
                else
                    repository.Update(tipo);

                context.SaveChanges();
            }
            model.id = tipo.id;
            return model;
            
        
        }


        public List<TipoModel> list() {
            List<TipoModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                TipoRepository repository = new TipoRepository(context);
                List<Tipo> list= repository.ListAll();
                listmodel = mapper.Map<List<TipoModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<TipoModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                TipoRepository repository = new TipoRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Tipo> tipo= repository.Recover(t=>t.id==id);
                return mapper.Map<Maybe<TipoModel>>(tipo);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                TipoRepository repository = new TipoRepository(context);
                Maybe<Tipo> tipo = repository.Recover(t => t.id == id);
                if (tipo.TryGetValue(out Tipo data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
