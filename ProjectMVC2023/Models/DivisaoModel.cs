﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class DivisaoModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }

        public Decimal escala { get; set; }
        
        public int restricao_idade { get; set; }



        public DivisaoModel save(DivisaoModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Divisao divisao = mapper.Map<Divisao>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                DivisaoRepository repository = new DivisaoRepository(context);

                if (model.id == 0)
                    repository.Insert(divisao);
                else
                    repository.Update(divisao);

                context.SaveChanges();
            }
            model.id = divisao.id;
            return model;
            
        
        }


        public List<DivisaoModel> list() {
            List<DivisaoModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                DivisaoRepository repository = new DivisaoRepository(context);
                List<Divisao> list= repository.ListAll();
                listmodel = mapper.Map<List<DivisaoModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<DivisaoModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                DivisaoRepository repository = new DivisaoRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Divisao> divisao= repository.Recover(d=>d.id==id);
                return mapper.Map<Maybe<DivisaoModel>>(divisao);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                DivisaoRepository repository = new DivisaoRepository(context);
                Maybe<Divisao> divisao = repository.Recover(d => d.id == id);
                if (divisao.TryGetValue(out Divisao data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
