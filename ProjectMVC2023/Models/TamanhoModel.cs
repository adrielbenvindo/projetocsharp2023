﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class TamanhoModel
    {
        [Display(Name = "Código")]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo obrigatório!")]
        [MaxLength(150,
            ErrorMessage ="Nome deve ter no máximo 150 caracteres")]
        [MinLength(3,
            ErrorMessage ="Nome deve ter no mínimo 3 caracteres")]
        [Display(Name ="Nome")]
        public String nome { get; set; }

        public int limite { get; set; }



        public TamanhoModel save(TamanhoModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Tamanho tamanho = mapper.Map<Tamanho>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                TamanhoRepository repository = new TamanhoRepository(context);

                if (model.id == 0)
                    repository.Insert(tamanho);
                else
                    repository.Update(tamanho);

                context.SaveChanges();
            }
            model.id = tamanho.id;
            return model;
            
        
        }


        public List<TamanhoModel> list() {
            List<TamanhoModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                TamanhoRepository repository = new TamanhoRepository(context);
                List<Tamanho> list= repository.ListAll();
                listmodel = mapper.Map<List<TamanhoModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<TamanhoModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                TamanhoRepository repository = new TamanhoRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Tamanho> tamanho= repository.Recover(t=>t.id==id);
                return mapper.Map<Maybe<TamanhoModel>>(tamanho);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                TamanhoRepository repository = new TamanhoRepository(context);
                Maybe<Tamanho> tamanho = repository.Recover(t => t.id == id);
                if (tamanho.TryGetValue(out Tamanho data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }
    }
}
