﻿using AutoMapper;
using Repository;
using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Models
{
    public class LoginModel
    {

        public int id { get; set; }

        public String username { get; set; }
        
        public String password { get; set; }
        




        public LoginModel save(LoginModel model) {

            //Tamanho cat = new Tamanho();
            //cat.id = model.id;
            //cat.descricao = model.descricao;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            Login login = mapper.Map<Login>(model);

            using (OrganizationContext context = new OrganizationContext())
            {
                LoginRepository repository = new LoginRepository(context);

                if (model.id == 0)
                    repository.Insert(login);
                else
                    repository.Update(login);

                context.SaveChanges();
            }
            model.id = login.id;
            return model;
            
        
        }


        public List<LoginModel> list() {
            List<LoginModel> listmodel = null;
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                LoginRepository repository = new LoginRepository(context);
                List<Login> list= repository.ListAll();
                listmodel = mapper.Map<List<LoginModel>>(list);
            }
            
            return listmodel;
        }

        public Maybe<LoginModel> select(int id) {
            var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
            using (OrganizationContext context = new OrganizationContext())
            {
                LoginRepository repository = new LoginRepository(context);
                //select * from categoria c where c.id = id
                Maybe<Login> login= repository.Recover(c=>c.id==id);
                return mapper.Map<Maybe<LoginModel>>(login);
            }
        }

        public void delete(int id) {

            using (OrganizationContext context = new OrganizationContext())
            {
                LoginRepository repository = new LoginRepository(context);
                Maybe<Login> login = repository.Recover(c => c.id == id);
                if (login.TryGetValue(out Login data)){
                    repository.Delete(data);
                    context.SaveChanges();
                }
            }
        }

        public Maybe<LoginModel> checkAcess(Maybe<String> username,Maybe<String> password) {
            LoginModel model = null;
            using (OrganizationContext contexto = new OrganizationContext())
            {
                LoginRepository repository = new LoginRepository(contexto);
                if (username.TryGetValue(out String usernameData) && password.TryGetValue(out String passwordData)){
                    Maybe<Login> login =repository.Recover(l=>l.username==usernameData && l.password==passwordData);
                    var mapper = new Mapper(AutoMapperConfig.RegisterMappings());
                    if (login.TryGetValue(out Login data))
                        model = mapper.Map<LoginModel>(data);
                }
                //select *from usuario where email=@email and senha=@senha
            }
            if (model == null)
                return new Maybe<LoginModel>.None();
            return model;

        }
    }
}
