﻿using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023
{
    public class FileHandle
    {
        public static Maybe<string> Upload(Maybe<IFormFile> file, Maybe<String> folder, Maybe<String> prefix, Maybe<String> sufix,
Maybe<IWebHostEnvironment> webHostEnvironment){
            string fileName = null;
            if (file.TryGetValue(out IFormFile fileData) && folder.TryGetValue(out String folderData) && prefix.TryGetValue(out String prefixData) &&
sufix.TryGetValue(out String sufixData) && webHostEnvironment.TryGetValue(out IWebHostEnvironment webHostEnvironmentData)){
                string folderFile = Path.Combine(webHostEnvironmentData.WebRootPath, folderData);
                fileName = Guid.NewGuid().ToString() + "_" + prefixData + fileData.FileName + "_" + sufixData;
                string filePath = Path.Combine(folderFile, fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create)){
                    fileData.CopyTo(fileStream);
                }
            }
            if (fileName == null)
                return new Maybe<string>.None();
            return Maybe.Some<string>(fileName);
        }
    }

}
