﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class TipoController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            return View(new TipoModel());
        }

        [HttpPost]
        public IActionResult save(TipoModel model) {
            if (ModelState.IsValid)
            {

                try
                {
                    TipoModel tipoModel = new TipoModel();
                    tipoModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list()
        {
            TipoModel tipoModel = new TipoModel();
            List<TipoModel> list = tipoModel.list();
            return View(list);//lista por parametro para a view
        }


        public IActionResult change(int id) {
            TipoModel model = new TipoModel();
            Maybe<TipoModel> ator = model.select(id);
            if(ator.TryGetValue(out TipoModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Tipo nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new TipoModel());       
            }
        }

        public IActionResult delete(int id) {
            TipoModel model = new TipoModel();
            try
            {
                Maybe<TipoModel> data = model.select(id);
                if(data is Maybe<TipoModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Tipo nao encontrado!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
