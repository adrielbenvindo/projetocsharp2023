﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class UsuarioController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            return View(new UsuarioModel());
        }

        [HttpPost]
        public IActionResult save(UsuarioModel model) {
            if (ModelState.IsValid)
            {

                try
                {
                    LoginModel loginModel = new LoginModel();
                    LoginModel login = new LoginModel();
                    login.id = model.id;
                    login.username = model.username;
                    login.password = model.password;
                    loginModel.save(login);
                    model.LoginId = login.id;
                    UsuarioModel usuarioModel = new UsuarioModel();
                    usuarioModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list()
        {
            UsuarioModel usuarioModel = new UsuarioModel();
            List<UsuarioModel> list = usuarioModel.list();
            return View(list);//lista por parametro para a view
        }


        public IActionResult change(int id) {
            UsuarioModel model = new UsuarioModel();
            Maybe<UsuarioModel> ator = model.select(id);
            if(ator.TryGetValue(out UsuarioModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Usuario nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new UsuarioModel());       
            }
        }

        public IActionResult delete(int id) {
            UsuarioModel model = new UsuarioModel();
            try
            {
                Maybe<UsuarioModel> data = model.select(id);
                if(data is Maybe<UsuarioModel>.Some){ 
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Usuario nao encontrado!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
