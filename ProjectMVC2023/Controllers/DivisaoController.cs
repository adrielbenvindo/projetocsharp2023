﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class DivisaoController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            return View(new DivisaoModel());
        }

        [HttpPost]
        public IActionResult save(DivisaoModel model) {
            if (ModelState.IsValid)
            {

                try
                {
                    DivisaoModel divisaoModel = new DivisaoModel();
                    divisaoModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list()
        {
            DivisaoModel divisaoModel = new DivisaoModel();
            List<DivisaoModel> list = divisaoModel.list();
            return View(list);//lista por parametro para a view
        }


        public IActionResult change(int id) {
            DivisaoModel model = new DivisaoModel();
            Maybe<DivisaoModel> divisao = model.select(id);
            if(divisao.TryGetValue(out DivisaoModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Ator nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new DivisaoModel());       
            }
        }

        public IActionResult delete(int id) {
            DivisaoModel model = new DivisaoModel();
            try
            {
                Maybe<DivisaoModel> data = model.select(id);
                if(data is Maybe<DivisaoModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Divisao nao encontrada!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
