﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class TamanhoController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            return View(new TamanhoModel());
        }

        [HttpPost]
        public IActionResult save(TamanhoModel model) {
            if (ModelState.IsValid)
            {

                try
                {
                    TamanhoModel tamanhomodel = new TamanhoModel();
                    tamanhomodel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list()
        {
            TamanhoModel tamanhoModel = new TamanhoModel();
            List<TamanhoModel> list = tamanhoModel.list();
            return View(list);//lista por parametro para a view
        }


        public IActionResult change(int id) {
            TamanhoModel model = new TamanhoModel();
            Maybe<TamanhoModel> ator = model.select(id);
            if(ator.TryGetValue(out TamanhoModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Tamanho nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new TamanhoModel());       
            }
        }

        public IActionResult delete(int id) {
            TamanhoModel model = new TamanhoModel();
            try
            {
                Maybe<TamanhoModel> data = model.select(id);
                if(data is Maybe<TamanhoModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Tamanho nao encontrado!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
