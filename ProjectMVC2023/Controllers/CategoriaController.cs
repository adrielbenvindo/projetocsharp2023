﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class CategoriaController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            return View(new CategoriaModel());
        }

        [HttpPost]
        public IActionResult save(CategoriaModel model) {
            if (ModelState.IsValid)
            {

                try
                {
                    CategoriaModel categoriaModel = new CategoriaModel();
                    categoriaModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list()
        {
            CategoriaModel categoriaModel = new CategoriaModel();
            List<CategoriaModel> list = categoriaModel.list();
            return View(list);//lista por parametro para a view
        }


        public IActionResult change(int id) {
            CategoriaModel model = new CategoriaModel();
            Maybe<CategoriaModel> categoria = model.select(id);
            if(categoria.TryGetValue(out CategoriaModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Categoria nao encontrada!";
                ViewBag.classe = "alert-danger";
                return View("cad", new CategoriaModel());       
            }
        }

        public IActionResult delete(int id) {
            CategoriaModel model = new CategoriaModel();
            try
            {
                Maybe<CategoriaModel> data = model.select(id);
                if(data is Maybe<CategoriaModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Categoria nao encontrada!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
