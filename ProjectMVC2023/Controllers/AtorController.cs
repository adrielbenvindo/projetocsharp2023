﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class AtorController : Controller
    {
        private IWebHostEnvironment webHostEnvironment;
        
        public AtorController(IWebHostEnvironment hostEnvironment)
        {
            webHostEnvironment = hostEnvironment;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad(int eventoId) {
            AtorModel model = new AtorModel();
            model.EventoId = eventoId;
            return View(model);
        }


       

        [HttpPost]
        public IActionResult save(AtorModel atorModel)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    atorModel.save(atorModel,webHostEnvironment);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", atorModel);
        }


        public IActionResult list(int eventoId)
        {
            AtorModel atorModel = new AtorModel();
            List<AtorModel> list = atorModel.listRelated(eventoId);
            ViewBag.eventoId = eventoId;
            return View(list);//list por parametro para a view
        }


        public IActionResult change(int id)
        {
            AtorModel model = new AtorModel();
            Maybe<AtorModel> ator = model.select(id);
            if(ator.TryGetValue(out AtorModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Ator nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new AtorModel());       
            }
        }

        public IActionResult delete(int id)
        {
            AtorModel model = new AtorModel();
            try
            {
                Maybe<AtorModel> data = model.select(id);
                if(data is Maybe<AtorModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Ator nao encontrado!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
