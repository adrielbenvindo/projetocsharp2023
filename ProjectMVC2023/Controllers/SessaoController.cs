﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class SessaoController : Controller
    {
        
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad(int eventoId) {
            SessaoModel model = new SessaoModel();
            model.EventoId = eventoId;
            List<SalaModel> list = (new SalaModel()).list();
            ViewBag.listsalas = list.Select(s=>new SelectListItem() {
               Value = s.id.ToString(), Text = s.nome
            });
            return View(model);
        }


       

        [HttpPost]
        public IActionResult save(SessaoModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    SessaoModel sessaoModel = new SessaoModel();
                    sessaoModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            List<SalaModel> list = (new SalaModel()).list();
            ViewBag.listsalas = list.Select(s=>new SelectListItem() {
               Value = s.id.ToString(), Text = s.nome
            });
            return View("cad", model);
        }


        public IActionResult list(int eventoId)
        {
            SessaoModel sessaoModel = new SessaoModel();
            List<SessaoModel> list = sessaoModel.listRelated(eventoId);
            ViewBag.eventoId = eventoId;
            return View(list);//list por parametro para a view
        }


        public IActionResult change(int id)
        {
            SessaoModel model = new SessaoModel();
            List<SalaModel> list = (new SalaModel()).list();
            ViewBag.listsalas = list.Select(s=>new SelectListItem() {
               Value = s.id.ToString(), Text = s.nome
            });
            Maybe<SessaoModel> ator = model.select(id);
            if(ator.TryGetValue(out SessaoModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Sessao nao encontrada!";
                ViewBag.classe = "alert-danger";
                return View("cad", new SessaoModel());       
            }
        }

        public IActionResult delete(int id)
        {
            SessaoModel model = new SessaoModel();
            try
            {
                Maybe<SessaoModel> data = model.select(id);
                if(data is Maybe<SessaoModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Sessao nao encontrada!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
