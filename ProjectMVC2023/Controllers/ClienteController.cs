﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class ClienteController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            return View(new ClienteModel());
        }

        [HttpPost]
        public IActionResult save(ClienteModel model) {
            if (ModelState.IsValid)
            {

                try
                {
                    LoginModel loginModel = new LoginModel();
                    LoginModel login = new LoginModel();
                    login.id = model.id;
                    login.username = model.username;
                    login.password = model.password;
                    loginModel.save(login);
                    model.LoginId = login.id;
                    ClienteModel clienteModel = new ClienteModel();
                    clienteModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list()
        {
            ClienteModel clienteModel = new ClienteModel();
            List<ClienteModel> list = clienteModel.list();
            return View(list);//lista por parametro para a view
        }


        public IActionResult change(int id) {
            ClienteModel model = new ClienteModel();
            Maybe<ClienteModel> cliente = model.select(id);
            if(cliente.TryGetValue(out ClienteModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Ator nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new ClienteModel());       
            }
        }

        public IActionResult delete(int id) {
            ClienteModel model = new ClienteModel();
            try
            {
                Maybe<ClienteModel> data = model.select(id);
                if(data is Maybe<ClienteModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Cliente nao encontrado!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
