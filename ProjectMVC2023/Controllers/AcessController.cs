﻿using Microsoft.AspNetCore.Mvc;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class AcessController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult login() {

            return View();
        }

        [HttpPost]
        public IActionResult check(String username,String password,Boolean cliente_check) {
            Maybe<LoginModel> model = (new LoginModel()).checkAcess(Maybe.Some<String>(username),Maybe.Some<String>(password));
            if(model.TryGetValue(out LoginModel data)){
                //encontrou
                //inseriu na sessão
                String name = "";
                int id = 0;
                if (cliente_check){
                    ClienteModel cliente_model = new ClienteModel();
                    Maybe<ClienteModel> result = cliente_model.selectByLogin(data.id);
                    if(result.TryGetValue(out ClienteModel resultData)){
                        name = resultData.nome;
                        id = resultData.id;
                    }else{
                        //não encontrou
                        ViewBag.mensagem = "Cadastro inexistente";
                        ViewBag.classe = "alert-danger";
                        return View("login");
                    }
                }else{
                    UsuarioModel usuario_model = new UsuarioModel();
                    Maybe<UsuarioModel> result = usuario_model.selectByLogin(data.id);
                    if(result.TryGetValue(out UsuarioModel resultData)){
                        name = resultData.nome;
                        id = resultData.id;
                    }else{
                        //não encontrou
                        ViewBag.mensagem = "Cadastro inexistente";
                        ViewBag.classe = "alert-danger";
                        return View("login");
                    }
                }
                HttpContext.Session.SetInt32("idUsuario", id);
                HttpContext.Session.SetString("nomeUsuario", name);
                return RedirectToAction("Index", "Home");
            }else{
                //não encontrou
                ViewBag.mensagem = "Dados inválidos";
                ViewBag.classe = "alert-danger";
                return View("login");
            }
        }

        public IActionResult logout() {
            //limpar a sessão
            HttpContext.Session.Remove("nomeUsuario");
            HttpContext.Session.Remove("idUsuario");
            HttpContext.Session.Clear();

            //redirecionar para login
            return RedirectToAction("login", "Acess");
        }

    }


}
