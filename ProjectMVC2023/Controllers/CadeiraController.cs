﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class CadeiraController : Controller
    {
        
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad(int salaId) {
            CadeiraModel model = new CadeiraModel();
            model.SalaId = salaId;
            return View(model);
        }


       

        [HttpPost]
        public IActionResult save(CadeiraModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    CadeiraModel cadeiraModel = new CadeiraModel();
                    cadeiraModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            return View("cad", model);
        }


        public IActionResult list(int salaId)
        {
            CadeiraModel cadeiraModel = new CadeiraModel();
            List<CadeiraModel> list = cadeiraModel.listRelated(salaId);
            ViewBag.salaId = salaId;
            return View(list);//list por parametro para a view
        }


        public IActionResult change(int id)
        {
            CadeiraModel model = new CadeiraModel();
            Maybe<CadeiraModel> cadeira = model.select(id);
            if(cadeira.TryGetValue(out CadeiraModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Cadeira nao encontrada!";
                ViewBag.classe = "alert-danger";
                return View("cad", new CadeiraModel());       
            }
        }

        public IActionResult delete(int id)
        {
            CadeiraModel model = new CadeiraModel();
            try
            {
                Maybe<CadeiraModel> data = model.select(id);
                if(data is Maybe<CadeiraModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Cadeira nao encontrada!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
