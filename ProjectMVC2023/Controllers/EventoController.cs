﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class EventoController : Controller
    {
        private IWebHostEnvironment webHostEnvironment;
        
        public EventoController(IWebHostEnvironment hostEnvironment)
        {
            webHostEnvironment = hostEnvironment;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            List<TipoModel> list = (new TipoModel()).list();
            ViewBag.listtipos = list.Select(t=>new SelectListItem() {
               Value = t.id.ToString(), Text = t.nome
            });
            return View(new EventoModel());
        }


       

        [HttpPost]
        public IActionResult save(EventoModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    EventoModel eventoModel = new EventoModel();
                    eventoModel.save(model,webHostEnvironment);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            List<TipoModel> list = (new TipoModel()).list();
            ViewBag.listtipos = list.Select(t=>new SelectListItem() {
               Value = t.id.ToString(), Text = t.nome
            });

            return View("cad", model);
        }


        public IActionResult list()
        {
            EventoModel eventoModel = new EventoModel();
            List<EventoModel> list = eventoModel.list();
            return View(list);//list por parametro para a view
        }


        public IActionResult change(int id)
        {
            EventoModel model = new EventoModel();
            List<TipoModel> list = (new TipoModel()).list();
            ViewBag.listtipos = list.Select(t=>new SelectListItem() {
               Value = t.id.ToString(), Text = t.nome
            });
            Maybe<EventoModel> tipo = model.select(id);
            if(tipo.TryGetValue(out EventoModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Ator nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new EventoModel());       
            }
        }

        public IActionResult delete(int id)
        {
            EventoModel model = new EventoModel();
            try
            {
                Maybe<EventoModel> data = model.select(id);
                if(data is Maybe<EventoModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Evento nao encontrado!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
