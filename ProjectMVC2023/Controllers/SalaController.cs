﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectMVC2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLib;

namespace ProjectMVC2023.Controllers
{
    public class SalaController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult cad() {
            List<TamanhoModel> list = (new TamanhoModel()).list();
            ViewBag.listtamanhos = list.Select(t=>new SelectListItem() {
               Value = t.id.ToString(), Text = t.nome + " - " + t.limite
            });
            return View(new SalaModel());
        }


       

        [HttpPost]
        public IActionResult save(SalaModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    SalaModel salaModel = new SalaModel();
                    salaModel.save(model);
                    ViewBag.mensagem = "Dados salvos com sucesso!";
                    ViewBag.classe = "alert-success";
                }
                catch (Exception ex)
                {

                    ViewBag.mensagem = "ops... Erro ao salvar!" + ex.Message + "/" + ex.InnerException;
                    ViewBag.classe = "alert-danger";
                }
            }
            else
            {
                ViewBag.mensagem = "ops... Erro ao salvar! verifique os campos";
                ViewBag.classe = "alert-danger";

            }

            List<TamanhoModel> list = (new TamanhoModel()).list();
            ViewBag.listtamanhos = list.Select(t => new SelectListItem()
            {
                Value = t.id.ToString(),
                Text = t.nome + " - " + t.limite
            });

            return View("cad", model);
        }


        public IActionResult list()
        {
            SalaModel salaModel = new SalaModel();
            List<SalaModel> list = salaModel.list();
            return View(list);//list por parametro para a view
        }


        public IActionResult change(int id)
        {
            SalaModel model = new SalaModel();
            List<TamanhoModel> list = (new TamanhoModel()).list();
            ViewBag.listtamanhos = list.Select(t => new SelectListItem()
            {
                Value = t.id.ToString(),
                Text = t.nome + " - " + t.limite
            });
            Maybe<SalaModel> sala = model.select(id);
            if(sala.TryGetValue(out SalaModel data))
                return View("cad", data);
            else{
                ViewBag.mensagem = "Ator nao encontrado!";
                ViewBag.classe = "alert-danger";
                return View("cad", new SalaModel());       
            }
        }

        public IActionResult delete(int id)
        {
            SalaModel model = new SalaModel();
            try
            {
                Maybe<SalaModel> data = model.select(id);
                if(data is Maybe<SalaModel>.Some){
                    model.delete(id);
                    ViewBag.mensagem = "Dados excluidos com sucesso!";
                    ViewBag.classe = "alert-success";
                }else{
                    ViewBag.mensagem = "Sala nao encontrada!";
                    ViewBag.classe = "alert-danger";
                }
            }
            catch (Exception ex)
            {

                ViewBag.mensagem = "Ops... Não foi possível excluir!" + ex.Message;
                ViewBag.classe = "alert-danger";
            }

            return View("list", model.list());
        }
    }
}
