using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class EventoCategoria
    {
        public int id { get; set; }
        public int EventoId { get; set; }
        public int CategoriaId { get; set; }
        public virtual Evento evento { get; set; }
        public virtual Categoria categoria { get; set; }

    }
}
