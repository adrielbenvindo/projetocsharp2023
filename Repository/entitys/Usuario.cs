using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
    public class Usuario
    {
        public Usuario() {
            this.eventos = new HashSet<Evento>();
        }
        public int id { get; set; }
        public String nome { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public bool is_admin { get; set; }
        public bool is_event_manager { get; set; }

        public int LoginId { get; set; }

        public virtual Login login { get; set; }
        public virtual ICollection<Evento> eventos { get; set; }
    }
}
