using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Cadeira
    {
        public Cadeira() {
            this.ingressos = new HashSet<Ingresso>();
            this.cadeira_divisoes = new HashSet<CadeiraDivisao>();
        }

        public int id { get; set; }
        public String nome { get; set; }
        public int SalaId { get; set; }
        public virtual Sala sala { get; set; }

        public virtual ICollection<Ingresso> ingressos { get; set; }
        public virtual ICollection<CadeiraDivisao> cadeira_divisoes { get; set; }

    }
}
