﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Sala
    {
        public Sala() {
            this.sessoes = new HashSet<Sessao>();
            this.cadeiras = new HashSet<Cadeira>();
        }

        public int id { get; set; }
        public String nome { get; set; }
        public int TamanhoId { get; set; }
        public virtual Tamanho tamanho { get; set; }

        public virtual ICollection<Sessao> sessoes { get; set; }
        public virtual ICollection<Cadeira> cadeiras { get; set; }

    }
}
