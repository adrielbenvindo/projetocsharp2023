using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
    public class Cliente
    {
        public Cliente() {
            this.ingressos = new HashSet<Ingresso>();
        }
        public int id { get; set; }
        public String nome { get; set; }
        public String email { get; set; }
        public String username { get; set; }
        public String password { get; set; }

        public int LoginId { get; set; }

        public virtual Login login { get; set; }
        public virtual ICollection<Ingresso> ingressos { get; set; }
    }
}
