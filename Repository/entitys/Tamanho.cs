﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
    public class Tamanho
    {
        public Tamanho() {
            this.salas = new HashSet<Sala>();
        }
        public int id { get; set; }
        public String nome { get; set; }
        public int limite { get; set; }

        public virtual ICollection<Sala> salas { get; set; }
    }
}
