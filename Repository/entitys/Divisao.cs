using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Divisao
    {
        public Divisao() {
            this.ingressos = new HashSet<Ingresso>();
            this.cadeira_divisoes = new HashSet<CadeiraDivisao>();
        }

        public int id { get; set; }
        public String nome { get; set; }
        public Decimal escala { get; set; }
        public int restricao_idade { get; set; }

        public virtual ICollection<Ingresso> ingressos { get; set; }
        public virtual ICollection<CadeiraDivisao> cadeira_divisoes { get; set; }

    }
}
