using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class CadeiraDivisao
    {
        public int id { get; set; }
        public int CadeiraId { get; set; }
        public int DivisaoId { get; set; }
        public virtual Cadeira cadeira { get; set; }
        public virtual Divisao divisao { get; set; }

    }
}
