using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Ingresso
    {
        public int id { get; set; }
        public Decimal valor { get; set; }
        public String identificador { get; set; }
        public int SessaoId { get; set; }
        public int CadeiraId { get; set; }
        public int ClienteId { get; set; }
        public int DivisaoId { get; set; }
        public virtual Sessao sessao { get; set; }
        public virtual Cadeira cadeira { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual Divisao divisao { get; set; }

    }
}
