using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
    public class Categoria
    {
        public Categoria() {
            this.evento_categorias = new HashSet<EventoCategoria>();
        }
        public int id { get; set; }
        public String nome { get; set; }

        public virtual ICollection<EventoCategoria> evento_categorias { get; set; }
    }
}
