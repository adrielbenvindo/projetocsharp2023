using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
    public class Tipo
    {
        public Tipo() {
            this.eventos = new HashSet<Evento>();
        }
        public int id { get; set; }
        public String nome { get; set; }

        public virtual ICollection<Evento> eventos { get; set; }
    }
}
