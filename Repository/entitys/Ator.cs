using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Ator
    {
        public int id { get; set; }
        public String nome { get; set; }
        public int idade { get; set; }
        public String papel { get; set; }
        public int EventoId { get; set; }
        
        public String foto { get; set; }

        public virtual Evento evento { get; set; }

    }
}
