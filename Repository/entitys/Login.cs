using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
    public class Login
    {
        public Login() {
            this.clientes = new HashSet<Cliente>();
            this.usuarios = new HashSet<Usuario>();
        }
        public int id { get; set; }
        public String username { get; set; }
        public String password { get; set; }

        public virtual ICollection<Cliente> clientes { get; set; }
        public virtual ICollection<Usuario> usuarios { get; set; }
    }
}
