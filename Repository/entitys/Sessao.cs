using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Sessao
    {
        public Sessao() {
            this.ingressos = new HashSet<Ingresso>();
        }

        public int id { get; set; }
        public String nome { get; set; }
        public Decimal valor { get; set; }
        public DateTime data_hora_inicio { get; set; }
        public DateTime data_hora_fim { get; set; }
        public int EventoId { get; set; }
        public int SalaId { get; set; }

        public virtual Evento evento { get; set; }
        public virtual Sala sala { get; set; }

        public virtual ICollection<Ingresso> ingressos { get; set; }

    }
}
