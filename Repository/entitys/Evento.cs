using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.entitys
{
   public class Evento
    {
        public Evento() {
            this.evento_categorias = new HashSet<EventoCategoria>();
            this.sessoes = new HashSet<Sessao>();
            this.atores = new HashSet<Ator>();
        }

        public int id { get; set; }
        public String nome { get; set; }
        public int duracao { get; set; }
        public int restricao_idade { get; set; }
        public int TipoId { get; set; }
        public int ResponsavelId { get; set; }
        
        public String cartaz { get; set; }

        public virtual Tipo tipo { get; set; }
        public virtual Usuario responsavel { get; set; }

        public virtual ICollection<EventoCategoria> evento_categorias { get; set; }
        public virtual ICollection<Sessao> sessoes { get; set; }
        public virtual ICollection<Ator> atores { get; set; }

    }
}
