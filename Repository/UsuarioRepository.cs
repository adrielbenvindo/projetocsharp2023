﻿
 
 using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using Repository.context;
 using Repository.entitys;
 
 namespace Repository
 {
   public class UsuarioRepository:BaseRepository<Usuario>
   {
     public UsuarioRepository(OrganizationContext context):base(context) { }
   }
 }
