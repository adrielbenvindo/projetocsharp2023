﻿using Repository.context;
using Repository.entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class SalaRepository:BaseRepository<Sala>
    {
        public SalaRepository(OrganizationContext context) : base(context)
        { 
        }
    }
}
