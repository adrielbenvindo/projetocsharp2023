﻿using Repository.entitys;
using Repository.context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataLib;

namespace Repository
{


    public abstract class BaseRepository<T> where T : class
    {

        protected OrganizationContext _context;

        private DbSet<T> _table;

        public BaseRepository(OrganizationContext context)
        {

            this._context = context;

            this._table = this._context.Set<T>();

        }

        public virtual void Insert(T entity)
        {

            this._table.Add(entity);

        }

        public virtual void Update(T entity)
        {

            this._context.Entry(entity).State = EntityState.Modified;

        }

        public virtual void Delete(T entity)
        {
            this._context.Entry(entity).State = EntityState.Deleted;
            this._table.Remove(entity);

        }
        
        public virtual List<T> List(Expression<Func<T, bool>> expressao)
        {

            return this._table.Where(expressao).ToList();

        }

        public virtual List<T> ListAll()
        {

            return this._table.ToList();

        }

        public virtual Maybe<T> Recover(Expression<Func<T, bool>> expression)
        {
            T data = this._table.Where(expression).SingleOrDefault();
            if (data == null)
                return new Maybe<T>.None();
            return Maybe.Some(data);

        }


    }
}
