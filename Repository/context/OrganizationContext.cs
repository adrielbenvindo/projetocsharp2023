﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repository.entitys;

namespace Repository.context
{
    public class OrganizationContext:DbContext
    {
        public OrganizationContext() {
            //criar ou atualizar o banco de dados
            this.Database.EnsureCreated();
        }
        public DbSet<Sala> sala { get; set; }
        public DbSet<Tamanho> tamanho { get; set; }
        public DbSet<Cadeira> cadeira { get; set; }
        public DbSet<CadeiraDivisao> cadeira_divisao { get; set; }
        public DbSet<Categoria> categoria { get; set; }
        public DbSet<Cliente> cliente { get; set; }
        public DbSet<Divisao> divisao { get; set; }
        public DbSet<Evento> evento { get; set; }
        public DbSet<Ator> ator { get; set; }
        public DbSet<EventoCategoria> evento_categoria { get; set; }
        public DbSet<Ingresso> ingresso { get; set; }
        public DbSet<Sessao> sessao { get; set; }
        public DbSet<Tipo> tipo { get; set; }
        public DbSet<Usuario> usuario { get; set; }
        public DbSet<Login> login { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // base.OnConfiguring(optionsBuilder);
            var stringConnection = @"Server=localhost;DataBase=Organization2023;trusted_connection=false;User Id=SA; Password=#135SqlserverAcess%; encrypt=False";

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(stringConnection);
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Login>(entity=> {
                entity.HasKey(t=>t.id);//chave primaria
                //qtde max caracteres
                entity.Property(t=>t.username).HasMaxLength(150);
                entity.Property(t=>t.password).HasMaxLength(500);

            });

            modelBuilder.Entity<Cliente>(entity => {
                entity.HasKey(c => c.id);
                entity.Property(c=>c.nome).HasMaxLength(150);
                entity.Property(c=>c.email).HasMaxLength(150);
                //relacionamento
                entity.HasOne(c => c.login) //prop lado um
                .WithMany(l => l.clientes) //prop lado Muitos
                .HasForeignKey(c => c.LoginId) //prop chave estrangeira
                .HasConstraintName("FK_Cliente_Login") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao

            });

            modelBuilder.Entity<Usuario>(entity => {
                entity.HasKey(u => u.id);
                entity.Property(u=>u.nome).HasMaxLength(150);
                //relacionamento
                entity.HasOne(u => u.login) //prop lado um
                .WithMany(l => l.usuarios) //prop lado Muitos
                .HasForeignKey(u => u.LoginId) //prop chave estrangeira
                .HasConstraintName("FK_Usuario_Login") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao

            });

            modelBuilder.Entity<Tamanho>(entity=> {
                entity.HasKey(t=>t.id);//chave primaria
                //qtde max caracteres
                entity.Property(t=>t.nome).HasMaxLength(150);

            });

            modelBuilder.Entity<Sala>(entity => {
                entity.HasKey(s =>s.id);
                entity.Property(s=>s.nome).HasMaxLength(150);
                //relacionamento
                entity.HasOne(s => s.tamanho) //prop lado um
                .WithMany(t => t.salas) //prop lado Muitos
                .HasForeignKey(s => s.TamanhoId) //prop chave estrangeira
                .HasConstraintName("FK_Sala_Tamanho") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao

            });

            modelBuilder.Entity<Divisao>(entity=> {
                entity.HasKey(d=>d.id);//chave primaria
                //qtde max caracteres
                entity.Property(d=>d.nome).HasMaxLength(150);
                entity.Property(d=>d.escala).HasPrecision(4,2);

            });

            modelBuilder.Entity<Categoria>(entity=> {
                entity.HasKey(c=>c.id);//chave primaria
                //qtde max caracteres
                entity.Property(c=>c.nome).HasMaxLength(150);

            });

            modelBuilder.Entity<Tipo>(entity=> {
                entity.HasKey(t=>t.id);//chave primaria
                //qtde max caracteres
                entity.Property(t=>t.nome).HasMaxLength(150);

            });

            modelBuilder.Entity<Evento>(entity => {
                entity.HasKey(e => e.id);
                entity.Property(e=>e.nome).HasMaxLength(150);
                //relacionamento
                entity.HasOne(e => e.tipo) //prop lado um
                .WithMany(t => t.eventos) //prop lado Muitos
                .HasForeignKey(e => e.TipoId) //prop chave estrangeira
                .HasConstraintName("FK_Evento_Tipo") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao
                entity.HasOne(e => e.responsavel)
                .WithMany(u => u.eventos)
                .HasForeignKey(e => e.ResponsavelId)
                .HasConstraintName("FK_Evento_Responsavel")
                .OnDelete(DeleteBehavior.NoAction);

            });
            
            modelBuilder.Entity<Ator>(entity => {
                entity.HasKey(a => a.id);
                entity.Property(a=>a.nome).HasMaxLength(150);
                entity.Property(a=>a.papel).HasMaxLength(150);
                //relacionamento
                entity.HasOne(a => a.evento) //prop lado um
                .WithMany(e => e.atores) //prop lado Muitos
                .HasForeignKey(a => a.EventoId) //prop chave estrangeira
                .HasConstraintName("FK_Evento_Ator") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao

            });

            modelBuilder.Entity<EventoCategoria>(entity => {
                entity.HasKey(ec =>ec.id);
                //relacionamento
                entity.HasOne(ec => ec.categoria)
                .WithMany(c => c.evento_categorias)
                .HasForeignKey(ec => ec.CategoriaId)
                .HasConstraintName("FK_Evento_Categoria_Categoria")
                .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(ec => ec.evento)
                .WithMany(e => e.evento_categorias)
                .HasForeignKey(ec => ec.EventoId)
                .HasConstraintName("FK_Evento_Categoria_Evento")
                .OnDelete(DeleteBehavior.NoAction);

            });

            modelBuilder.Entity<Sessao>(entity => {
                entity.HasKey(s => s.id);
                entity.Property(s=>s.nome).HasMaxLength(150);
                entity.Property(s=>s.valor).HasPrecision(8,2);
                //relacionamento
                entity.HasOne(s => s.sala) //prop lado um
                .WithMany(sa => sa.sessoes) //prop lado Muitos
                .HasForeignKey(s => s.SalaId) //prop chave estrangeira
                .HasConstraintName("FK_Sala_Sessao") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao
                entity.HasOne(s => s.evento)
                .WithMany(e => e.sessoes)
                .HasForeignKey(s => s.EventoId)
                .HasConstraintName("FK_Evento_Sessao")
                .OnDelete(DeleteBehavior.NoAction);

            });

            modelBuilder.Entity<Cadeira>(entity => {
                entity.HasKey(c =>c.id);
                entity.Property(c=>c.nome).HasMaxLength(150);
                //relacionamento
                entity.HasOne(c => c.sala) //prop lado um
                .WithMany(s => s.cadeiras) //prop lado Muitos
                .HasForeignKey(c => c.SalaId) //prop chave estrangeira
                .HasConstraintName("FK_Cadeira_Sala") //nome do relacionamento
                .OnDelete(DeleteBehavior.NoAction); //configuração da exclusao

            });

            modelBuilder.Entity<CadeiraDivisao>(entity => {
                entity.HasKey(cd =>cd.id);
                //relacionamento
                entity.HasOne(cd => cd.divisao)
                .WithMany(d => d.cadeira_divisoes)
                .HasForeignKey(cd => cd.DivisaoId)
                .HasConstraintName("FK_Cadeira_Divisao_Divisao")
                .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(cd => cd.cadeira)
                .WithMany(c => c.cadeira_divisoes)
                .HasForeignKey(cd => cd.CadeiraId)
                .HasConstraintName("FK_Cadeira_Divisao_Cadeira")
                .OnDelete(DeleteBehavior.NoAction);

            });

            modelBuilder.Entity<Ingresso>(entity => {
                entity.HasKey(i => i.id);
                entity.Property(i=>i.valor).HasPrecision(8,2);
                entity.Property(i=>i.identificador).HasMaxLength(150);
                //relacionamento
                entity.HasOne(i => i.sessao)
                .WithMany(s => s.ingressos)
                .HasForeignKey(i => i.SessaoId)
                .HasConstraintName("FK_Ingresso_Sessao")
                .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(i => i.cadeira)
                .WithMany(ca => ca.ingressos)
                .HasForeignKey(i => i.CadeiraId)
                .HasConstraintName("FK_Ingresso_Cadeira")
                .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(i => i.cliente)
                .WithMany(cl => cl.ingressos)
                .HasForeignKey(i => i.ClienteId)
                .HasConstraintName("FK_Ingresso_Cliente")
                .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(i => i.divisao)
                .WithMany(d => d.ingressos)
                .HasForeignKey(i => i.DivisaoId)
                .HasConstraintName("FK_Ingresso_Divisao")
                .OnDelete(DeleteBehavior.NoAction);

            });

        }
    }
}
